/**
 * @file main.cpp
 * @brief Template code for building with Shunya Stack
 * 
 * Compilation: Code comes with a cmake file, just run cmake 
 * 
 * Usage : Just run the command './main'
 */

/* --- Standard Includes --- */
#include <iostream>
#include <cstdlib>
#include <stdint.h>
#include <time.h> 
#include <unistd.h>
#include <errno.h>

#include <opencv2/opencv.hpp>

/* --- RapidJSON Includes --- */
/* MANDATORY: Allows to parse Shunya AI binaries output */
#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/istreamwrapper.h"
#include "rapidjson/stringbuffer.h"
#include "rapidjson/ostreamwrapper.h"
#include "rapidjson/filereadstream.h"

#include "subprocess.hpp" /* MANDATORY: Allows to run Shunya AI binaries */
#include "exutils.h" /* MANDATORY: Allows to parse Shunya AI binaries output */


/* --- Shunya Interfaces Includes --- */
#include <si/shunyaInterfaces.h> /* MANDATORY: Contains all IoT Functions */
#include <si/video.h>
#include <si/whatsapp.h> 

using namespace std;
using namespace rapidjson;

int main(void)
{
    /* MANDATORY: Initializes the Shunya components */
    initLib();

    /* Write your code here */
    return 0;
}